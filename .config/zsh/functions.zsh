# vim:ft=zsh

# Function to source files if they exist
# function zsh_add_file() {
#     local zsh_file_path="${ZDOTDIR:-.}/$1"
#     [ -f "$zsh_file_path" ] && command source "$zsh_file_path" && return 0
#     [ -f "$1" ] && command source "$1" && return 0
#     return 1
# }
function zsh_add_file() {
    local file_path="$1"

    if [ -f "$file_path" ]; then
        # echo "Sourcing file: $file_path"
        . "$file_path"
        return 0
    elif [ -f "${ZDOTDIR:-.}/$file_path" ]; then
        file_path="${ZDOTDIR:-.}/$file_path"
        # echo "Sourcing file: $file_path"
        . "$file_path"
        return 0
    fi

    echo "File not found: $file_path"
    return 1
}


# Function to load specific plugin if it exists or download from Github
function zsh_add_plugin() {
    local PLUGIN_NAME
    local REPO_URL

    # Extract the plugin name from the input
    if [[ "$1" == *"/"* ]]; then
        PLUGIN_NAME=$(echo "$1" | cut -d "/" -f 2)
        REPO_URL="https://gitlab.com/$1.git"
    else
        PLUGIN_NAME="$1"
        REPO_URL="https://github.com/$1.git"
    fi

    # Define plugin paths
    local ZPLUGIN_PATHS=("/usr/share/zsh/plugins/$PLUGIN_NAME" "$ZDOTDIR/plugins/$PLUGIN_NAME" "/usr/local/share/$PLUGIN_NAME")

    # Loop through paths
    for zsh_plugin_path in "${ZPLUGIN_PATHS[@]}"; do
        if [ -d "$zsh_plugin_path" ]; then
            for file_type in "plugin.zsh" "zsh"; do
                local zsh_file_path="$zsh_plugin_path/${PLUGIN_NAME}.$file_type"
                if [ -f "$zsh_file_path" ]; then
                    # echo "Sourcing plugin file: $zsh_file_path"
                    . "$zsh_file_path"
                    return 0
                fi
            done
        fi
    done

    # Clone the repository
    local clone_path="$ZDOTDIR/plugins/$PLUGIN_NAME"
    git clone "$REPO_URL" "$clone_path"

    # Source the plugin files
    for file_type in "plugin.zsh" "zsh"; do
        local zsh_file_path="$clone_path/${PLUGIN_NAME}.$file_type"
        if [ -f "$zsh_file_path" ]; then
            # echo "Sourcing plugin file after cloning: $zsh_file_path"
            . "$zsh_file_path"
            return 0
        fi
    done

    echo "Plugin files not found after cloning: $1"
    return 1
}

# Function to add completions
function zsh_add_completion() {
    PLUGIN_NAME=$(echo $1 | cut -d "/" -f 2)
    if [ -d "$ZDOTDIR/plugins/$PLUGIN_NAME" ]; then
        # For completions
		completion_file_path=$(ls $ZDOTDIR/plugins/$PLUGIN_NAME/_*)
		fpath+="$(dirname "${completion_file_path}")"
        zsh_add_file "plugins/$PLUGIN_NAME/$PLUGIN_NAME.plugin.zsh"
    else
        git clone "https://github.com/$1.git" "$ZDOTDIR/plugins/$PLUGIN_NAME"
		fpath+=$(ls $ZDOTDIR/plugins/$PLUGIN_NAME/_*)
        [ -f $ZDOTDIR/.zccompdump ] && $ZDOTDIR/.zccompdump
    fi
	completion_file="$(basename "${completion_file_path}")"
	if [ "$2" = true ] && compinit "${completion_file:1}"
}

