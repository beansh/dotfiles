# FZF
# TODO update for mac
# on mac /usr/local/opt/fzf/shell/{completions.zsh, key-bindings.zsh}
#[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh
#[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
#[ -f /usr/share/doc/fzf/examples/completion.zsh ] && source /usr/share/doc/fzf/examples/completion.zsh
#[ -f /usr/share/doc/fzf/examples/key-bindings.zsh ] && source /usr/share/doc/fzf/examples/key-bindings.zsh
#[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
#[ -f $ZDOTDIR/completion/_fnm ] && fpath+="$ZDOTDIR/completion/"
# export FZF_DEFAULT_COMMAND='rg --hidden -l ""'

####
# OS-SPECIFIC
####

case "$OSTYPE" in
  darwin*)
        _fzf_path='/usr/local/opt/fzf/shell'
  ;;
  linux*)
        _fzf_path='/usr/share/fzf'
  ;;
  *)
        # _fzf_path=' ...'
  ;;
esac

check_and_source() { [ -f "$1" ] && source "$1"; }

check_and_source "$_fzf_path/completion.zsh"
check_and_source "$_fzf_path/key-bindings.zsh"

unset _fzf_path

check_and_source "$ZDOTDIR/completion/_fnm" && fpath+="$ZDOTDIR/completion/"
export FZF_DEFAULT_COMMAND='rg --hidden -l ""'

