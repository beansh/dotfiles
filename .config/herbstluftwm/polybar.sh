#!/usr/bin/env bash
# Simple script to launch polybar on two displays

# Quit polybar if it is running
[ ! -z "$(pgrep -u $UID -x polybar)" ] && polybar-msg cmd quit

# Wait for running polybar to actually quit
while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done

# Stow away last polybar log file
[ -f /tmp/polybar-$USER.log ] && mv /tmp/polybar-$USER.log /tmp/polybar-$USER.log.old

# Define a function to launch polybar
# Use desktop_session and ostype to declare 
launch_bar () {
	ds=$DESKTOP_SESSION
# Default is linux, but need to handle things differently for FreeBSD
# Detect BSD system and launch a (*-bsd) bar variant
	case "$OSTYPE" in
		*bsd*)	os="bsd";;
		*)		os="";;
	esac
# Add $os to command line only when necessary
	MONITOR=$1 polybar --reload bar-$ds-${os:+$os-}$2 -c $HOME/.config/polybar/$ds/config.ini -l warning 2>&1 | tee -a /tmp/polybar-$USER.log & disown
}

# check if there is more than one display, but skip the last
# polybar -m | head -n -1 | awk -F : '{print $1}'

# for display in $(polybar -m | head -n -1 | awk -F : '{print $1}'); do
# 	launch_bar $display
# done

display1="$(polybar -m | awk -F : 'NR==1 {print $1}')"
display2="$(polybar -m | head -n -1 | awk -F : 'NR==2 {print $1}')"

# Launch polybar on first two displays
launch_bar $display1 1
[ ! -z "$display2" ] && launch_bar $display2 2

