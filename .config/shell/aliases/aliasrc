# vim:ft=zsh
# Useful aliases for bash & zsh

alias e="$EDITOR"
alias v="$EDITOR"

# run specific editor
alias n="nvim"
alias m="micro"

# Use neovim for vim if present.
[ -x "$(command -v nvim)" ] && alias vim="nvim" vimdiff="nvim -d"

# update zsh plugins
#DEPRECATED: using Zap
#alias zsh-update-plugins="find "$ZDOTDIR/plugins" -type d -exec test -e '{}/.git' ';' -print0 | xargs -I {} -0 git -C {} pull -q"

# Handy VIM commands to exit shell
alias :q!="exit"
alias :wq="exit"
alias :q="exit"
alias :Q="exit"

# fix shell word expansion for sudo
# https://wiki.archlinux.org/title/Sudo#Passing_aliases
alias sudo='sudo '

# colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# easier to read disk
alias df='df -h'     # human-readable sizes
alias free='free -m' # show sizes in MB

# get top process eating memory
alias psmem='ps auxf | sort -nr -k 4 | head -5'

# get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3 | head -5'

# kitty
kitty-reload() { kill -SIGUSR1 $(pidof kitty) }
alias s='kitty +kitten ssh'

# list files nicely
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -alFh'
alias l='ls'
alias l.="ls -A | egrep '^\.'"

# fix obvious typo's
alias cd..='cd ..'
alias pdw='pwd'

# colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# readable output
alias df='df -h'

# continue download
alias wget="wget -c"

# userlist
alias userlist="cut -d: -f1 /etc/passwd"

# ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

# switch between bash and zsh
alias tobash="sudo chsh -s \$(command -v bash) \$USER && echo 'Now log out.'"
alias tozsh="sudo chsh -s \$(command -v zsh) \$USER && echo 'Now log out.'"
alias tofish="sudo chsh -s \$(command -v zsh) \$USER && echo 'Now log out.'"

# youtube download
alias yta-aac="yt-dlp --extract-audio --audio-format aac "
alias yta-best="yt-dlp --extract-audio --audio-format best "
alias yta-flac="yt-dlp --extract-audio --audio-format flac "
alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "
alias ytv-best="yt-dlp -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4 "

# search content with ripgrep
alias rg="rg --sort path"

# gpg
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias fix-gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias fix-gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# shutdown or reboot
alias ssn="sudo shutdown now"
alias sr="reboot"

# git
alias rmgitcache="rm -r ~/.cache/git"
alias grh="git reset --hard"

# ssh-keygen
alias ssh-newkey="ssh-keygen -C "$(whoami)@$(uname -n)-$(date -I)" -t ed25519"

# sshfs allow root to access mounts and keep connections alive
# REF: https://serverfault.com/questions/6709/sshfs-mount-that-survives-disconnect
alias sshfs="sshfs -o allow_other,reconnect,ServerAliveInterval=15,ServerAliveCountMax=3"

# import dotfile aliases
source ~/.config/shell/aliases/dots.aliasrc

# import OS-Specific aliases
case "$OSTYPE" in
  solaris*)
	echo "No available aliases for SOLARIS"
	;;
  darwin*)  
	# echo "Setting up aliases for OSX"
	source ~/.config/shell/aliases/mac.aliasrc
	;; 
  linux*)
	# echo "Setting up aliases for LINUX"
	source ~/.config/shell/aliases/linux.aliasrc
	;;
  *bsd*)
	# echo "Setting up aliases for BSD" 
	source ~/.config/shell/aliases/bsd.aliasrc
	;;
  msys*)
	echo "No available aliases for WINDOWS" 
	;;
  cygwin*)
	echo "No available aliases for cyg-WINDOWS" 
	;;
  *)
	echo "unknown: $OSTYPE" 
	;;
esac

