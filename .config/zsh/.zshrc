####
# GLOBAL CONFIGURATION
####

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Source profile if not sourced already by WM 
[ -z $XDG_CACHE_HOME ] && [ -f ~/.config/shell/profile ] && source ~/.config/shell/profile

# Setup history and store in cache directory:
HISTSIZE=10000000
SAVEHIST=10000000
[ ! -d "$XDG_CACHE_HOME"/zsh ] && mkdir -p "$XDG_CACHE_HOME"/zsh
HISTFILE="$XDG_CACHE_HOME"/zsh/history
# append to history file rather than replacing
setopt APPEND_HISTORY
# Perform textual history expansion, csh-style, treating the character ‘!’ specially.
setopt BANG_HIST				# Treat the '!' character specially during expansion.
setopt HIST_EXPIRE_DUPS_FIRST	# Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS			# Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS		# Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS		# Do not display a line previously found.
setopt HIST_IGNORE_SPACE		# Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS		# Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS		# Remove superfluous blanks before recording entry.
setopt HIST_VERIFY				# Don't execute immediately upon history expansion.
setopt SHARE_HISTORY			#share commands between terminal instances or not

# some useful options (man zshoptions)

# If a command is issued that can’t be executed as a normal command, and the command is the name of a directory,
# perform the cd command to that directory. This option is only applicable if the option SHIN_STDIN is set, i.e. if
# commands are being read from standard input. The option is designed for interactive use; it is recommended that
# cd be used explicitly in scripts to avoid ambiguity.
setopt AUTO_CD

# Treat the ‘#’, ‘~’ and ‘^’ characters as part of patterns for filename generation, etc.
# (An initial unquoted ‘~’ always produces named directory expansion.)
setopt EXTENDED_GLOB

# If a pattern for filename generation has no matches, print an error, instead of leaving it
# unchanged in the argument list. This also applies to file expansion of an initial ‘~’ or ‘=’.
setopt NOMATCH

# On an ambiguous completion, instead of listing possibilities or beeping, insert the first
# match immediately. Then when completion is requested again, remove the first match and insert
# the second match, etc. When there are no more matches, go back to the first one again.
# reverse-menu-complete may be used to loop through the list in the other direction.
# This option overrides AUTO_MENU.
setopt MENU_COMPLETE

# Do not require a leading ‘.’ in a filename to be matched explicitly.
#setopt GLOB_DOTS

# Useful for debugging
# setopt SOURCE_TRACE

# Allow comments even in interactive shells.
setopt INTERACTIVE_COMMENTS

stty stop undef		# Disable ctrl-s to freeze terminal.

zle_highlight=('paste:none')

# beeping is annoying
unsetopt BEEP

# completions
autoload -Uz compinit
# use a completion menu instead of going through each match blindly
# filter the completion menu itself using the completion system
zstyle ':completion:*' menu select 
# cache completion to speed up commands
# move zcompcache to cache folder
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache

# TODO: figure out what this means, til then disable it
# zstyle ':completion::complete:lsof:*' menu yes select

# REF: https://zsh.sourceforge.io/Doc/Release/Completion-Widgets.html#Completion-Matching-Control
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'

zmodload zsh/complist
_comp_options+=(globdots) # Include hidden files.
# load compinit at the end
zle_highlight=('paste:none')

# Colors
autoload -Uz colors && colors

# DEPRECATED: replaced by Zap
# Useful Functions
# source "$ZDOTDIR/functions.zsh"

# Shell aliases
source "$XDG_CONFIG_HOME/shell/aliases/aliasrc"

# Add keybindings
source "$ZDOTDIR/keybindings.zsh"

# Add vim-mode related keybindings
source "$ZDOTDIR/vim-mode.zsh"

# # Source other files in $ZDOTDIR using zsh_add_file() from functions.zsh 
# zsh_add_file "vim-mode.zsh"
# zsh_add_file "prompt-new.zsh"
# zsh_add_file "fzf.zsh"

# Plugins
# zsh_add_plugin "fast-syntax-highlighting"
# zsh_add_plugin "zsh-autosuggestions"
# source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh
# echo "autosuggestions loaded"
# source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# echo "syntax-highlighing loaded"

# Zap, a minimalist plugin manager
# Upstream URL: https://github.com/zap-zsh/zap
# install Zap but keep original .zshrc with -k option
# zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.zsh) --branch release-v1 -k

# Source Zap
# Install Zap if it is not installed
get_zap() {
    local _ZAP="${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh"
    local _ZAPINST="zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.zsh) --branch release-v1 -k"
    
    # Check if the zap file exists; if not, install zap automatically
    [ ! -f "$_ZAP" ] && eval "$_ZAPINST"
}

# Call get_zap function to ensure zap is installed
get_zap

# Check that Zap is installed then source it
[ -f "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh" ] && source "${XDG_DATA_HOME:-$HOME/.local/share}/zap/zap.zsh"

# Zap install plugins
plug "hlissner/zsh-autopair"
plug "zsh-users/zsh-completions"
plug "zsh-users/zsh-history-substring-search"

# Zap supercharge
# Dont install but integrate into config:
# completions setopt HIST keybindings colors exports bindkey compinit
# plug "zap-zsh/supercharge"

# Source local files with Zap

# Prompt
plug "zap-zsh/zap-prompt"
# plug "$ZDOTDIR/prompt-new.zsh"

plug "$ZDOTDIR/fzf.zsh"

# yabai completetions (mac only)
[[ $OSTYPE = darwin* ]] && plug "Amar1729/yabai-zsh-completions"

# Plugin order suggestion: Have syntax highlighting at the end, then autosuggestions after
# Syntax highlighting
plug "zsh-users/zsh-syntax-highlighting"
# plug "z-shell/F-Sy-H"

# Autosuggestions
plug "zsh-users/zsh-autosuggestions"

# Run compinit
# REF: https://gist.github.com/ctechols/ca1035271ad134841284
# On slow systems, checking the cached .zcompdump file to see if it must be 
# regenerated adds a noticable delay to zsh startup.  This little hack restricts 
# it to once a day.  It should be pasted into your own completion file.
#
# The globbing is a little complicated here:
# - '#q' is an explicit glob qualifier that makes globbing work within zsh's [[ ]] construct.
# - 'N' makes the glob pattern evaluate to nothing when it doesn't match (rather than throw a globbing error)
# - '.' matches "regular files"
# - 'mh+24' matches files (or directories or whatever) that are older than 24 hours.
# for dump in "$XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION"(N.mh+24); do
#   compinit
# done

# As of zsh version 5.9 the -i option "removes insecure elements from the set of completion functions, where previously it ignored the compaudit result and included all elements". Better to use the -u option to skip security checks, especially when running a Homebrew-installed zsh as a separate user with a non-admin account.
# WORKAROUND: Ignore security check because we are not the homebrew admin. Check with compaudit
#compinit -u -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION
[[ $OSTYPE = darwin* ]] && compinit -u -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION || compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION

# Launch Starship prompt
# eval "$(starship init zsh)"

