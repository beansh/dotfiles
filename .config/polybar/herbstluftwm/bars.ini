################################################################################
################################################################################
############                        MAINBAR                         ############
################################################################################
################################################################################

[bar/main]

; Use either of the following command to list available outputs:
; $ polybar -M | cut -d ':' -f 1
; $ xrandr -q | grep " connected" | cut -d ' ' -f1
; If no monitor is given, the primary monitor is used if it exists
monitor = ${env:MONITOR}

; Use the specified monitor as a fallback if the main one is not found.
monitor-fallback = LVDS-0

; Require the monitor to be in connected state
monitor-strict = false

; Use fuzzy matching for monitors (only ignores dashes -)
; Useful when monitors are named differently with different drivers.
monitor-exact = true

; Tell the Window Manager not to configure the window.
; Use this to detach the bar if your WM is locking its size/position.
; Note: With this most WMs will no longer reserve space for 
; the bar and it will overlap other windows. You need to configure
; your WM to add a gap where the bar will be placed.
override-redirect = true

; Put the bar at the bottom of the screen
bottom = false

; Prefer fixed center position for the `modules-center` block. 
; The center block will stay in the middle of the bar whenever
; possible. It can still be pushed around if other blocks need
; more space.
; When false, the center block is centered in the space between 
; the left and right block.
fixed-center = true

; Width and height of the bar window.
; Supports any percentage with offset or an extent value.
; For 'width', the percentage is relative to the monitor width and for 'height'
; relative to the monitor height
width = 100%
height = 2.2%

; Offset the bar window in the x and/or y direction.
; Supports any percentage with offset relative to the monitor width (offset-x)
; or height (offset-y)
;offset-x = 1%
;offset-y = 1%

; Background ARGB color (e.g. #f00, #ff992a, #ddff1023)
background = ${colors.background}

; Foreground ARGB color (e.g. #f00, #ff992a, #ddff1023)
foreground = ${colors.foreground}

; Background gradient (vertical steps)
;   background-[0-9]+ = #aarrggbb
; background-0 =

; Value used for drawing rounded corners
; For this to work you may also need to enable pseudo-transparency or use a
; compositor like picom.
; Individual values can be defined using:
;   radius-{top,bottom}
; or
;   radius-{top,bottom}-{left,right} (New in version 3.6.0)
; Polybar always uses the most specific radius definition for each corner.
radius = 9.0

; Under-/overline pixel size and argb color
; Individual values can be defined using:
;   {overline,underline}-size
;   {overline,underline}-color
line-size = 2
line-color = #f00

; Values applied to all borders
; Individual side values can be defined using:
;   border-{left,top,right,bottom}-size
;   border-{left,top,right,bottom}-color
; The top and bottom borders are added to the bar height, so the effective
; window height is:
;   height + border-top-size + border-bottom-size
; Meanwhile the effective window width is defined entirely by the width key and
; the border is placed within this area. So you effectively only have the
; following horizontal space on the bar:
;   width - border-right-size - border-left-size
; border-size supports any percentage with offset.
; For border-{left,right}-size, the percentage is relative to the monitor width
; and for border-{top,bottom}-size, it is relative to the monitor height.
border-size = 4
; border-left-size = 2
; border-right-size = 2
; border-top-size = 2
; border-bottom-size = 2
; border-color = ${colors.foreground}

; Padding (number of spaces, pixels, or points) to add at the beginning/end of
; the bar
; Individual side values can be defined using:
;   padding-{left,right}
padding-left = 1
padding-right = 1

; Margin (number of spaces, pixels, or points) to add before/after each module
; Individual side values can be defined using:
;   module-margin-{left,right}
module-margin-left = 1
module-margin-right = 1

; Fonts are defined using <font-name>;<vertical-offset>
; Font names are specified using a fontconfig pattern.
;   font-0 = NotoSans-Regular:size=8;2
;   font-1 = MaterialIcons:size=10
;   font-2 = Termsynu:size=8;-1
;   font-3 = FontAwesome:size=10
; See the Fonts wiki page for more details
;https://github.com/jaagr/polybar/wiki/Fonts
font-0 = "SF Pro Display:style=Bold:size=9;0"
; Emoji support for wttr
font-1 = "JoyPixels:style=Regular:scale=12:antialias=false;-1"

; Modules are added to one of the available blocks
;   modules-left = cpu ram
;   modules-center = xwindow xbacklight
;   modules-right = ipc clock
modules-left = xkeyboard menu ewmh xwindow
modules-center = system-cpu-frequency cpu temperature memory filesystem
modules-right = arch-aur-updates pulseaudio wttr date tray

; The separator will be inserted between the output of each module
; This has the same properties as a label
separator = 

; Opacity value between 0.0 and 1.0 used on fade in/out
dim-value = 1.0

; Value to be used to set the WM_NAME atom
; If the value is empty or undefined, the atom value
; will be created from the following template: polybar-[BAR]_[MONITOR]
; NOTE: The placeholders are not available for custom values
wm-name =

; Locale used to localize various module data (e.g. date)
; Expects a valid libc locale, for example: sv_SE.UTF-8
locale = de_CH-gsw.UTF-8
;de_CH.utf8
;wae_CH.utf8

; Restack the bar window and put it above the
; selected window manager's root
;
; Fixes the issue where the bar is being drawn
; on top of fullscreen window's
;
; Currently supported values:
;   generic (Moves the bar window above the first window in the window stack.
;            Works in xmonad, may not work on other WMs)
;           (New in version 3.6.0)
;   bspwm
;   i3 (requires `override-redirect = true`)
wm-restack = bspwm

; Set a DPI values used when rendering text
; This only affects scalable fonts
; Set this to 0 to let polybar calculate the dpi from the screen size.
; dpi = 
; dpi-x = 96
; dpi-y = 96

; Enable support for inter-process messaging
; See the Messaging wiki page for more details.
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left = 
click-middle = 
click-right =
scroll-up =
scroll-down =
double-click-left =
double-click-middle =
double-click-right =
;scroll-up = herbstclient use_index -1
;scroll-down = herbstclient use_index +1

; If two clicks are received within this interval (ms), they are recognized as
; a double click.
; New in version 3.6.0
double-click-interval = 400

; Requires polybar to be built with xcursor support (xcb-util-cursor)
; Possible values are:
; - default   : The default pointer as before, can also be an empty string (default)
; - pointer   : Typically in the form of a hand
; - ns-resize : Up and down arrows, can be used to indicate scrolling
cursor-click = 
cursor-scroll =

################################################################################

[bar/bar-herbstluftwm-extra]
inherit = bar/main
bottom = true
modules-left = load-average
modules-center = networkspeeddown networkspeedup
modules-right = cpu2 temperature temperature-gpu memory2 filesystem

################################################################################

[bar/bar-herbstluftwm-1]
inherit = bar/main

################################################################################

[bar/bar-herbstluftwm-2]
inherit = bar/main
tray-position = none
modules-center = network
modules-right = pulseaudio date changebackground

################################################################################

[bar/bar-herbstluftwm-bsd-1]
inherit = bar/main
modules-center = cpu memory
modules-right = backlight-acpi pulseaudio wttr date changebackground tray
locale = de_CH.UTF-8
