# Key-bindings

typeset -g -A key

key[Delete]="${terminfo[kdch1]}"
key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"

[[ -n "${key[Delete]}" ]] && bindkey -- "${key[Delete]}" delete-char
[[ -n "${key[Home]}" ]] && bindkey -- "${key[Home]}" beginning-of-line
[[ -n "${key[End]}" ]] && bindkey -- "${key[End]}" end-of-line
[[ -n "${key[PageUp]}" ]] && bindkey -- "${key[PageUp]}" history-search-backward
[[ -n "${key[PageDown]}" ]] && bindkey -- "${key[PageDown]}" history-search-forward
[[ -n "${key[Up]}" ]] && bindkey -- "${key[Up]}" up-line-or-history
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-history
[[ -n "${key[Left]}" ]] && bindkey -- "${key[Left]}" backward-char
[[ -n "${key[Right]}" ]] && bindkey -- "${key[Right]}" forward-char

# autocompletion using arrow keys (based on history)
bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward

# delete - delete one char forward
bindkey '^[[3~' delete-char

# Ctrl + Backspace to delete a whole word.
bindkey '^H' backward-kill-word

# home - go to the beginning of line
bindkey '^[[H' beginning-of-line

# end - go to the end of line
bindkey '^[[F' end-of-line

# page up
bindkey '^[[5~' history-search-backward

# page down
bindkey '^[[6~' history-search-forward

# Ctrl + Left/Right move to beginning of prev/next word
key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

[[ -n "${key[Control-Left]}"  ]] && bindkey -- "${key[Control-Left]}"  backward-word
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word

# Reload zshrc
bindkey -s '^x' '^usource $ZSHRC\n'
