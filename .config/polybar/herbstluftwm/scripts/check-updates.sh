#!/bin/sh
# Super simple update checker for Arch and AUR packages
#
# Check for updates
updates_arch=$(checkupdates 2> /dev/null | wc -l) || updates_arch=0
updates_aur=$(paru -Qum 2> /dev/null | wc -l) || updates_aur=0

# Print update count
echo "ARCH: $updates_arch AUR: $updates_aur"
