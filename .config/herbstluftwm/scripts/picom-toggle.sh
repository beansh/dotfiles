#!/usr/bin/env bash

# Toggle compfy or picom

toggle() {
	local comp cmdline

	# Check if it is installed
	command -v compfy &>/dev/null && comp="compfy" && cmdline="compfy -b --config ~/.config/compfy/compfy.conf" ||
	command -v picom &>/dev/null && comp="picom" && cmdline="picom -b --config ~/.config/picom/picom.conf" ||
	{ echo "No compositor installed"; return 1; }

	pgrep -x "$comp" &>/dev/null && echo "Shutting down $comp..." && killall "$comp" ||
	{ echo "Starting $comp..."; eval "$cmdline"; }
}

toggle
