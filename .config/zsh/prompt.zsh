# vim:ft=zsh

# autoload -Uz promptinit
# promptinit

# Determine icon based on OS
case "$OSTYPE" in
	darwin*)
		# echo "OS is macOS"
		#_icon="%F{green}🍎%f"
        _icon="%F{$(id -Gn $USER | grep -q -w staff && echo green || echo red)}🍎%f"
	;;
	linux*)
		# echo "OS is Linux"
		_icon="%F{cyan}%f"
	;;
	*)
		echo "OS is unknown"
		_icon=""
	;;
esac

# Define host information and current working directory
_hostinfo="%F{blue}%n%f on %F{green}%m%f"
_cwd="%F{cyan}%c%f"

# Autoload vcs and colors
autoload -Uz vcs_info
autoload -U colors && colors

# Set up vcs_info to check for changes and format git information
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
# zstyle ':vcs_info:git:*' formats " %r/%S %b %m%u%c "
zstyle ':vcs_info:git:*' formats " %{$fg[blue]%}(%{$fg[red]%}%m%u%c%{$fg[yellow]%}%{$fg[magenta]%} %b%{$fg[blue]%})"

# Setup hook that runs before every prompt
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst

# Function to check for untracked files in the Git directory
# from https://github.com/zsh-users/zsh/blob/master/Misc/vcs_info-examples
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
#
+vi-git-untracked(){
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
        git status --porcelain | grep '??' &> /dev/null ; then
        # This will show the marker if there are any untracked files in repo.
        # If instead you want to show the marker only if there are untracked
        # files in $PWD, use:
        #[[ -n $(git ls-files --others --exclude-standard) ]] ; then
        hook_com[staged]+='!' # signify new files with a bang
    fi
}


# Define prompt status
_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )%{$fg[cyan]%}%"

# Debug statements for prompt components
# echo "_icon: $_icon"
# echo "_hostinfo: $_hostinfo"
# echo "_cwd: $_cwd"
# echo "_status: $_status"

# Prompt setup
PROMPT="$_icon $_hostinfo $_cwd $_status"

# echo "Script ran successfully"

