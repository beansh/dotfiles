# Dotfiles

Shell configuration files

# Features

Zsh: .config/zsh

Neovim: .config/nvim

Kitty: .config/kitty

Herbstluftwm: .config/herbstluftwm

Sxhkd: .config/sxhkd 

Polybar: .config/polybar

# Dependencies
lxsession
kitty
dunst
neovim
keepassxc
apple-fonts
wallutils
solaar
jgmenu
compfy
fzf
zsh-autosuggestions-git
zsh-fast-syntax-highlighting
zsh-history-substring-search

## Clone to new machine

Clone the dotfiles into a bare repository.

```bash
[ -z $XDG_STATE_HOME ] && export XDG_STATE_HOME="$HOME/.local/state"
[ ! -d $XDG_STATE_HOME ] && mkdir -p $XDG_STATE_HOME
git clone --bare https://gitlab.com/username/dotfiles.git $XDG_STATE_HOME/dotfiles
```

Checkout dotfiles content from the bare repository to `$HOME`.

```bash
git --git-dir=$XDG_STATE_HOME/dotfiles/ --work-tree=$HOME checkout
```

Put away conflicting dotfiles if checkout fails, then re-run checkout.

```bash
[ ! -d ~/.default-dotfile-backup ] && mkdir ~/.default-dotfile-backup
alias dotfiles='git --git-dir=$XDG_STATE_HOME/dotfiles/ --work-tree=$HOME'
dotfiles status | egrep "deleted:" | awk {'print $2'} | xargs -I{} mv {} $HOME/.default-dotfile-backup/
dotfiles checkout
```

## Bare repository setup

Setup a bare repository for git to track changes.
I use $XDG_STATE_HOME/dotfiles ($HOME/.local/state/dotfiles) to keep the $HOME directory tidy and keep the bare git repo folder out of the way.

```bash
[ -z $XDG_STATE_HOME ] && export XDG_STATE_HOME="$HOME/.local/state"
[ ! -d $XDG_STATE_HOME ] && mkdir -p $XDG_STATE_HOME
git init --bare $XDG_STATE_HOME/dotfiles
```

Make a shell alias to tell Git where the history and the working tree live.

```bash
alias dotfiles='git --git-dir=$XDG_STATE_HOME/dotfiles/ --work-tree=$HOME'
```

Tell Git not to show every file under `$HOME` when running `dotfiles status`.

```bash
dotfiles config status.showUntrackedFiles no
```

Set up the remote repository

```bash
dotfiles remote add origin https://gitlab.com/username/dotfiles.git
```

## Manage dotfiles

```bash
dotfiles status
dotfiles add ~/.config/shell/aliasrc
dotfiles commit -m "alias config"
dotfiles push origin main
```

